package tw.com.timecalc.timecalc;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MainActivity extends Activity {
    private Context context = MainActivity.this;
    private EditText editCurTime;
    private EditText editAddTime;
    private TextView txtResult;
    private Button btnCurTime;
    private Button btnAddTime;

    private boolean isValidTime(String time) {
        if(time.matches("^(([0-9])|([0-1][0-9])|([2][0-3])):(([0-9])|([0-5][0-9]))$")) {
            return true;
        } else {
            return false;
        }
    }

    private int getMins(String addTime) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Calendar cal = Calendar.getInstance();
            cal.setTime(df.parse(addTime));
            return cal.get(Calendar.MINUTE) + cal.get(Calendar.HOUR_OF_DAY) * 60;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private String newTime(String curTime, int numMins) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            Calendar cal = Calendar.getInstance();

            cal.setTime(df.parse(curTime));
            cal.add(Calendar.MINUTE, numMins);
            return df.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String curTime = editCurTime.getText().toString();
            String addTime = editAddTime.getText().toString();
            if(isValidTime(curTime) && isValidTime(addTime)) {
//                Log.e("dd", "-> " + getMins(addTime));
                txtResult.setText(newTime(curTime, getMins(addTime)));
            } else if(isValidTime(curTime) && (addTime.equals("") || addTime.equals("0"))) {
                StringBuffer sb = new StringBuffer();
                sb.append("+00:30 = " + newTime(curTime, 30 * 1) + "<br/>");
                sb.append("+01:00 = " + newTime(curTime, 30 * 2) + "<br/>");
                sb.append("+01:30 = " + newTime(curTime, 30 * 3) + "<br/>");
                sb.append("+02:00 = " + newTime(curTime, 30 * 4) + "<br/>");
                sb.append("+02:30 = " + newTime(curTime, 30 * 5) + "<br/>");
                sb.append("+03:00 = " + newTime(curTime, 30 * 6) + "<br/>");
                sb.append("+03:30 = " + newTime(curTime, 30 * 7) + "<br/>");
                sb.append("+04:00 = " + newTime(curTime, 30 * 8) + "<br/>");
                sb.append("+05:00 = " + newTime(curTime, 30 * 10) + "<br/>");
                sb.append("+06:00 = " + newTime(curTime, 30 * 12) + "<br/>");
                sb.append("+07:00 = " + newTime(curTime, 30 * 14) + "<br/>");
                sb.append("+08:00 = " + newTime(curTime, 30 * 16) + "<br/>");
                sb.append("+09:00 = " + newTime(curTime, 30 * 18) + "<br/>");
                sb.append("+10:00 = " + newTime(curTime, 30 * 20) + "<br/>");
                sb.append("+11:00 = " + newTime(curTime, 30 * 22) + "<br/>");
                sb.append("+12:00 = " + newTime(curTime, 30 * 24) + "<br/>");
                sb.append("+13:00 = " + newTime(curTime, 30 * 26) + "<br/>");
                sb.append("+14:00 = " + newTime(curTime, 30 * 28) + "<br/>");
                sb.append("+15:00 = " + newTime(curTime, 30 * 30) + "<br/>");
                sb.append("+16:00 = " + newTime(curTime, 30 * 32) + "<br/>");
                sb.append("+17:00 = " + newTime(curTime, 30 * 34) + "<br/>");
                sb.append("+18:00 = " + newTime(curTime, 30 * 36) + "<br/>");
                sb.append("+19:00 = " + newTime(curTime, 30 * 38) + "<br/>");
                sb.append("+20:00 = " + newTime(curTime, 30 * 40) + "<br/>");
                sb.append("+21:00 = " + newTime(curTime, 30 * 42) + "<br/>");
                sb.append("+22:00 = " + newTime(curTime, 30 * 44) + "<br/>");
                sb.append("+23:00 = " + newTime(curTime, 30 * 46) + "<br/>");
                sb.append("+24:00 = " + newTime(curTime, 30 * 48) + "<br/>");
                sb.append("+48:00 = " + newTime(curTime, 30 * 96) + "<br/>");
                txtResult.setText(Html.fromHtml(sb.toString()));
            } else {
                txtResult.setText(getString(R.string.wrong_format));
            }
        }
    };

    View.OnClickListener curTimePickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Calendar c = Calendar.getInstance();
            int mHour = c.get(Calendar.HOUR_OF_DAY);
            int mMinute = c.get(Calendar.MINUTE);
            TimePickerDialog tpd = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    editCurTime.setText(hourOfDay + ":" + minute);
                }
            }, mHour, mMinute, false);
            tpd.show();
        }
    };

    View.OnClickListener addTimePickerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int mHour = 1;
            int mMinute = 0;
            TimePickerDialog tpd = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    editAddTime.setText(hourOfDay + ":" + minute);
                }
            }, mHour, mMinute, true);
            tpd.setTitle(getString(R.string.addTime));
            tpd.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editCurTime = (EditText) findViewById(R.id.editCurTime);
        editAddTime = (EditText) findViewById(R.id.editAddTime);
        txtResult = (TextView) findViewById(R.id.txtResult);
        btnCurTime = (Button) findViewById(R.id.btnCurTime);
        btnAddTime = (Button) findViewById(R.id.btnAddTime);
        btnCurTime.setOnClickListener(curTimePickerListener);
        btnAddTime.setOnClickListener(addTimePickerListener);

        editCurTime.addTextChangedListener(textWatcher);
        editAddTime.addTextChangedListener(textWatcher);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String curTime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
        editCurTime.setText(curTime);
        editAddTime.setText("");
    }
}
